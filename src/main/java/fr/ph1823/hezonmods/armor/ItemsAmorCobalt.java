package fr.ph1823.hezonmods.armor;

import fr.ph1823.hezonmods.HezonMain;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

import net.minecraft.world.World;

/**
 * Created by ph1823 on 24/10/2017.
 * Pris du tu de MFF ( https://github.com/FFMT/ModTutoriel17/commit/e9f749f4a2c44c4962e7284ced353daf68a7895f )
 */
public class ItemsAmorCobalt  extends ItemArmor
{
    public ItemsAmorCobalt(ItemArmor.ArmorMaterial material, int type)
    {
        super(material, 0, type);
    }

    public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)
    {
        if(slot == 2)
        {
            return "hezon:textures/models/armor/diamond_layer_2.png";
        }
        return "hezon:textures/models/armor/diamond_layer_1.png";
    }

    public boolean getIsRepairable(ItemStack input, ItemStack repair)
    {
        if(repair.getItem() == HezonMain.cobalt)
        {
            return true;
        }
        return false;
    }

    public void onArmorTick(World world, EntityPlayer player, ItemStack stack)
    { //Effet quand on porte l'armure
       /* if(this.armorType == 0 && world.getBlockLightValue(MathHelper.floor_double(player.posX), MathHelper.floor_double(player.posY), MathHelper.floor_double(player.posZ)) < 8)
        {
            player.addPotionEffect(new PotionEffect(Potion.nightVision.id, 220, 0));
        }
        if(this.armorType == 2 && player.isSprinting())
        {
            player.motionX *= 1.1F;
            player.motionZ *= 1.1F;
        }
        player.addPotionEffect(new PotionEffect(Potion.resistance.id, 20, 0));*/
    }
}
