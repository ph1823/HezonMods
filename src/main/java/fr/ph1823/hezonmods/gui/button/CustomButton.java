package fr.ph1823.hezonmods.gui.button;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import fr.ph1823.hezonmods.gui.game.GuiMainMenuCustom;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.EnumChatFormatting;
import org.lwjgl.opengl.GL11;

import java.net.UnknownHostException;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

public class CustomButton extends GuiButton {
    private static final ThreadPoolExecutor thread = new ScheduledThreadPoolExecutor(5, (new ThreadFactoryBuilder()).setNameFormat("Server Pinger #%d").setDaemon(true).build());
    private ServerData serverData = new ServerData("MyLife", "minecraft127.omgserv.com:10756", true);
    private GuiMainMenuCustom menu;
    Random r = new Random();
    int r1;
    String color;

    public CustomButton(int id, int x, int y, int sizeX, int sizeY, GuiMainMenuCustom menu) {
        super(id, x, y, sizeX, sizeY, "");
        this.r1 = this.r.nextInt(EnumChatFormatting.values().length - 1);
        this.color = EnumChatFormatting.values()[this.r1].getFormattingCode() + "";
        this.menu = menu;
    }

    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        if (!this.serverData.field_78841_f)
        {
            this.serverData.field_78841_f = true;
            this.serverData.pingToServer = -2L;
            this.serverData.serverMOTD = "";
            this.serverData.populationInfo = "";
            thread.submit(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        CustomButton.this.menu.getPinger().func_147224_a(CustomButton.this.serverData);
                    }
                    catch (UnknownHostException unknownhostexception)
                    {
                        CustomButton.this.serverData.pingToServer = -1L;
                        CustomButton.this.serverData.serverMOTD = EnumChatFormatting.DARK_RED + "Can\'t resolve hostname";
                    }
                    catch (Exception exception)
                    {
                        CustomButton.this.serverData.pingToServer = -1L;
                        CustomButton.this.serverData.serverMOTD = EnumChatFormatting.DARK_RED + "Can\'t connect to server.";
                    }
                }
            });
        }

        if (this.visible)
        {
            FontRenderer fontrenderer = mc.fontRenderer;
            mc.getTextureManager().bindTexture(buttonTextures);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
            int k = this.getHoverState(this.field_146123_n);
            GL11.glEnable(GL11.GL_BLEND);
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, 46 + k * 20, this.width / 2, this.height);
            this.drawTexturedModalRect(this.xPosition + this.width / 2, this.yPosition, 200 - this.width / 2, 46 + k * 20, this.width / 2, this.height);
            this.mouseDragged(mc, mouseX, mouseY);
            int l = 14737632;

            if (packedFGColour != 0)
            {
                l = packedFGColour;
            }
            else if (!this.enabled)
            {
                l = 10526880;
            }
            else if (this.field_146123_n)
            {
                l = 16777120;
            }


            if(serverData.populationInfo != null && !serverData.populationInfo.isEmpty())
            {
                this.drawCenteredString(fontrenderer, "Hezon (" + serverData.populationInfo + ")" , this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, l);
            }
            else
            {
                this.drawCenteredString(fontrenderer, "Hzeon (...)" , this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, l);
            }
        }
    }
}
