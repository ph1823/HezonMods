package fr.ph1823.hezonmods.gui.register;

import cpw.mods.fml.common.network.IGuiHandler;
import fr.ph1823.hezonmods.gui.container.ContainerCompressor;
import fr.ph1823.hezonmods.gui.other.GuiCompressor;
import fr.ph1823.hezonmods.tileentity.TileEntityCompressed;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler{
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
       switch (ID) {
           case 0:
              TileEntityCompressed tileEntityFurnace = (TileEntityCompressed) world.getTileEntity(x, y, z);
              return new ContainerCompressor(player.inventory, tileEntityFurnace);

  /*        1:
           break;*/
           default:
               return null;

       }

    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case 0:
                TileEntityCompressed tileEntityFurnace = (TileEntityCompressed) world.getTileEntity(x, y, z);
                return new GuiCompressor(player.inventory, tileEntityFurnace);

  /*        1:
           break;*/
            default:
                return null;

        }
    }
}
