package fr.ph1823.hezonmods;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ph1823.hezonmods.armor.ItemsAmorCobalt;
import fr.ph1823.hezonmods.armor.ItemsAmorToxic;
import fr.ph1823.hezonmods.gui.game.GuiMainMenuCustom;
import fr.ph1823.hezonmods.gui.register.GuiHandler;
import fr.ph1823.hezonmods.items.CobaltsItem;
import fr.ph1823.hezonmods.items.ore.CobaltOre;
import fr.ph1823.hezonmods.items.ore.ToxicOre;
import fr.ph1823.hezonmods.items.other.CompressorBlock;
import fr.ph1823.hezonmods.items.sticks.IronStick;
import fr.ph1823.hezonmods.tileentity.TileEntityCompressed;
import fr.ph1823.hezonmods.utils.LogsUtils;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemSword;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;

/**
 * Created by ph1823 on 24/10/2017.
 */
@Mod(modid="hezon", name="Hezonmod", version="1.0")
public class HezonMain {

    public static HezonMain instance;

    //Variable utils
    public LogsUtils log=new LogsUtils();

    //Variable block

    //Toxic
    public static Block toxicOre;

    //Cobalt
    public static Block cobaltOre;

    //Compressed
    public static Block compressedBlock;
    //Défracteur
    public static Block defractor;

    //Hezonite
  //  public static Block hezoniteOre;

    //Variable Item

    //Cobalt
    public static Item cobalt, helmetCobalt,chestPlateCobalt,leggingsCobalt,bootsCobalt,cobaltSword;//Armures & items en cobalt

    //Toxic
    public static Item toxic, helmetToxic,chestPlateToxic,leggingsToxic,bootsToxic,toxicSword;

    public ItemArmor.ArmorMaterial cobaltAmor= EnumHelper.addArmorMaterial("armorCobalt", 25, new int[] {4, 9, 7, 4}, 20);


    //Item Hezonite
    public static Item hezonite=new Item().setUnlocalizedName("hezonite");

    public ItemArmor.ArmorMaterial tocAmor= EnumHelper.addArmorMaterial("armorToxic", 25, new int[] {6, 10, 8, 6}, 30);
    public static Item.ToolMaterial toxicM= EnumHelper.addToolMaterial("toxicMaterial",0,200,0,10,20);
    public static Item.ToolMaterial cobaltM= EnumHelper.addToolMaterial("cobaltM",0,200,0,10,20);
    private Item ironStick;

    //Event avant initialisation
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {

        instance=this;
        log.info("Démarrage de l'event \"preInit\" ");


        //TileEntiy Register
        GameRegistry.registerTileEntity(TileEntityCompressed.class,"compress");

        //Block Register
        compressedBlock=new CompressorBlock(true).setCreativeTab(CreativeTabs.tabBlock);//si redstone ou chez po
        GameRegistry.registerBlock(compressedBlock,"compressorBlock");


            //Cobalt
            cobaltOre=new CobaltOre(Material.rock);

            //Toxic
           toxicOre=new ToxicOre(Material.rock);

            //Hezonite
         //   hezoniteOre=new HezoniteOre();

        //Item register
            //Cobalt Item
            cobalt=new CobaltsItem().setTextureName("hezon:cobalt");
            GameRegistry.registerItem(cobalt,"cobalt");

            cobaltSword=new ItemSword(cobaltM).setUnlocalizedName("cobaltSword").setTextureName("cobaltSword");
             GameRegistry.registerItem(cobaltSword,"cobaltSword");

            helmetCobalt=new ItemsAmorCobalt(cobaltAmor,0)
                    .setUnlocalizedName("helmetCobalt")
                    .setTextureName("hezon:cobalt_helmet");
            GameRegistry.registerItem(helmetCobalt,"helmetCobalt");



            chestPlateCobalt=new ItemsAmorCobalt(cobaltAmor,1)
                    .setUnlocalizedName("chestPlateCobalt")
                    .setTextureName("hezon:cobalt_chestplate");
            GameRegistry.registerItem(chestPlateCobalt,"chestPlateCobalt");

            leggingsCobalt=new ItemsAmorCobalt(cobaltAmor,2)
                    .setUnlocalizedName("leggingsCobalt")
                    .setTextureName("hezon:cobalt_leggings");
            GameRegistry.registerItem(leggingsCobalt,"leggingsCobalt");

            bootsCobalt=new ItemsAmorCobalt(cobaltAmor,3)
                    .setUnlocalizedName("bootsCobalt")
                    .setTextureName("hezon:cobalt_boots");
            GameRegistry.registerItem(bootsCobalt,"bootsCobalt");
            //Toxic Item


            toxic=new Item().setUnlocalizedName("toxic").setTextureName("hezon:toxic");
            GameRegistry.registerItem(toxic,"toxic");

            toxicSword=new ItemSword(toxicM).setUnlocalizedName("toxicSword").setTextureName("toxicSword");
            GameRegistry.registerItem(toxicSword,"toxicSword");

            helmetToxic=new ItemsAmorToxic(tocAmor,0)
                    .setUnlocalizedName("helmetToxic")
                    .setTextureName("hezon:toxic_helmet");
            GameRegistry.registerItem(helmetToxic,"helmetToxic");



            chestPlateToxic=new ItemsAmorToxic(tocAmor,1)
                    .setUnlocalizedName("chestPlateToxic")
                    .setTextureName("hezon:toxic_chestplate");
            GameRegistry.registerItem(chestPlateToxic,"chestPlateToxic");

            leggingsToxic=new ItemsAmorToxic(tocAmor,2)
                    .setUnlocalizedName("leggingsToxic")
                    .setTextureName("hezon:toxic_leggings");
            GameRegistry.registerItem(leggingsToxic,"leggingsToxic");

            bootsToxic=new ItemsAmorToxic(tocAmor,3)
                    .setUnlocalizedName("bootsToxic")
                    .setTextureName("hezon:toxic_boots");
            GameRegistry.registerItem(bootsToxic,"bootsToxic");

            //Other Items
            hezonite=new Item().setUnlocalizedName("hezonite").setTextureName("hezonite");
            ironStick=new IronStick();






        log.info("Fin de l'event \"preInit\" ");
    }

    //Event pendant initilisation
    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
        //Register events
        if(event.getSide().isClient())
        {
            FMLCommonHandler.instance().bus().register(this);
        }

    }

    //Event après initialisation
    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public void onTick(TickEvent.ClientTickEvent event)
    {
        Minecraft mc = FMLClientHandler.instance().getClient();
        if(mc.currentScreen != null && mc.currentScreen.getClass().equals(GuiMainMenu.class))
        {
            mc.displayGuiScreen(new GuiMainMenuCustom());
        }
    }




}
