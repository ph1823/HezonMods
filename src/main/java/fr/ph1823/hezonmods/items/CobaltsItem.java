package fr.ph1823.hezonmods.items;

import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Created by ph1823 on 24/10/2017.
 */
public class CobaltsItem extends Item {

    public CobaltsItem() {
        setCreativeTab(CreativeTabs.tabMisc);
        setMaxStackSize(64);
        setUnlocalizedName("cobalt");

    }

}
