package fr.ph1823.hezonmods.items.sticks;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class HealStick extends Item{

    public HealStick() {
        setTextureName("healStick");
        setUnlocalizedName("healStick");
    }

    public boolean onItemUse(ItemStack item, EntityPlayer p, World w, int p_77648_4_, int p_77648_5_, int p_77648_6_, int p_77648_7_, float p_77648_8_, float p_77648_9_, float p_77648_10_)
    {

        p.addPotionEffect(new PotionEffect(6,20,1));

        return true;
    }

}
