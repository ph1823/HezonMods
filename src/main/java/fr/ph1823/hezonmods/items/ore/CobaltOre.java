package fr.ph1823.hezonmods.items.ore;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class CobaltOre extends Block {
    public CobaltOre(Material p_i45394_1_) {
        super(p_i45394_1_);
        setBlockName("cobaltOre");
        setBlockTextureName("cobaltOre");
    }
}
