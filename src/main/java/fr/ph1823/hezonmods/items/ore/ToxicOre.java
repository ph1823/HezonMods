package fr.ph1823.hezonmods.items.ore;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class ToxicOre extends Block {
    public ToxicOre(Material p_i45394_1_) {
        super(p_i45394_1_);
        setBlockName("toxicOre");
        setBlockTextureName("toxicOre");
    }
}
