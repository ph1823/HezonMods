package fr.ph1823.hezonmods.utils;

import cpw.mods.fml.common.FMLLog;

/**
 * Created by ph1823 on 24/10/2017.
 */
public class LogsUtils {

    public LogsUtils() {

    }

    public void info(String msg) {
        FMLLog.info("==== §b§lHezonInfo ====");
        FMLLog.info(msg);
        FMLLog.info("==== HezonEnd ===");
    }
    public void warn(String msg) {
        FMLLog.warning("==== §b§lHezonInfo ====");
        FMLLog.warning(msg);
        FMLLog.warning("==== HezonEnd ===");
    }
    public void bigwarn(String msg) {
        FMLLog.bigWarning("==== §b§lHezonInfo ===="+
                "\n" + msg
                + "\n" + "==== HezonEnd ===");
    }

}
